<?php
namespace let\composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

/**
* The basic implementaion, composer docs said we should implement 
* at least activate method to get it works as a plugin
*
*/
class VirginInstallerPlugin implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new VirginInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }
}