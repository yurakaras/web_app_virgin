<?php
namespace let\composer;
/**
 * Project: _tmp
 *
 * @author Yuri Efimov
 * @email yura.karas@gmail.com
 * @git  https://bitbucket.org/yurakaras
 * @date 27/01/14  22:48
 * 
 */



use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;

class VirginInstaller extends LibraryInstaller {
    private $packageName = 'yurakaras/webappvirgin';


    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath(PackageInterface $package)
    {
        $isValid = strpos($package->getName(), $this->packageName);
        if ($isValid=== false) {
            throw new \InvalidArgumentException(
                'Unable to install virgin app, '
                .' wrong package name '
            );
        }

        return 'mypath';
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'yurakaras-webappvirgin' === $packageType;
    }
} 