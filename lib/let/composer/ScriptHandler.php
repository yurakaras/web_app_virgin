<?php
namespace let\composer;
/**
 * Project: _tmp
 *
 * @author Yuri Efimov
 * @email yura.karas@gmail.com
 * @git  https://bitbucket.org/yurakaras
 * @date 27/01/14  06:19
 * 
 */


use Composer\Package\PackageInterface;

use Composer\Script\Event;

class ScriptHandler
{
    public static function preInstallCmd(Event $event)
    {
        $io = $event->getIO();
        $io->write('Welcome to Letcrew CMF composer installer');

        if ( $io->askConfirmation('Do u wanna it installed? ')) {
            return true;
        }
        $io->write('what a loss');
        exit;
        // make cache toasty
    }

    public static function preStatusCmd(Event $event)
    {
        // make cache toasty
    }

    public static function postStatusCmd(Event $event)
    {
        // make cache toasty
    }

    public static function preUpdateCmd(Event $event)
    {
        $composer = $event->getComposer();

        // do stuff
    }
    public static function postUpdateCmd(Event $event)
    {
        $composer = $event->getComposer();

        // do stuff
    }
    public static function prePackageInstall(Event $event)
    {
        $installedPackage = $event->getOperation()->getPackage();
        Echo("\n".$installedPackage.' installed'."\n");
    }

    public static function postPackageInstall(Event $event)
    {

        $installedPackage = $event->getOperation()->getPackage();
        $path = $event->getComposer()->getInstallationManager()->getInstallPath($installedPackage);
        /**@var $installedPackage PackageInterface **/

        $name = $installedPackage->getName();

        if ($name=="yurakaras/webappvirgin") {
            //$targetDir = $installedPackage->getTargetDir();
            var_dump($path);
        }

    }


    public static function postInstallCmd(Event $event)
    {
        // make cache toasty
    }
}