## to reveal composer power ##
### install sample package to custom dir (not vendor) ###
#### yii virgin app structure ####

-----------

#### Por que? ####
    I want to install my package through composer, but not to the common "vendor" dir so i digged it and here it is...
  
0.0.3 starts here
  
* Better create a composer.json, than try include it as a repo, with url and type:zip
* I need a to create custom installer class which implements Composer\Installer\InstallerInterface called it let\composer\VirginInstaller
* Probably i need also to implement a VirginInstallerPlugin (already done), but i try not to use it, as do not consider my package as 'composer-plugin' at all
* Just need the installer to work - changes in composer.json - type is the main thing to follow


------------

Version 0.0.2

    
I tried to load this repo just as plain zip package, but install it not to vendor folder
Few lines from composer.json from new project

    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "yurakaras/webappvirgin",
                "version": "master",
                "type": "yurakaras-webappvirgin",
                "dist": {
                    "url": "https://bitbucket.org/yurakaras/web_app_virgin/get/master.zip",
                    "type": "zip"
                },
                "extra": {
                    "class": "Let\\composer\\VirginInstaller"
                },
                "require": {
                    "let/composer/installers": "~1.0"
                }
            }
        },
        {
            "type": "composer",
            "url": "http://packages.phundament.com"
        }
    ],
    "extra": {
        "class": "Let\\composer\\VirginInstaller"
    },
    
It didn't work out nicely
- - -

